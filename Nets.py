#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 21 10:50:53 2020

@author: at-lab
"""

import torch
import torch.nn as nn

import torch.utils.data as data
import torch.autograd as autograd
import torch.nn.init as weight_init
import torchvision.transforms as transforms
from torch.optim.optimizer import Optimizer, required
import sys, os
import numpy as np

_DEVICE=torch.device("cuda" if torch.cuda.is_available() else "cpu")

class PermutationLayer(autograd.Function):     
    @staticmethod
    def forward(ctx, x, p, coeff, RowCon, ColCon, DerivRow, DerivCol, SharpCon, DerivSharp):                
        ctx.save_for_backward(x, p, DerivCol, DerivRow, SharpCon, DerivSharp, coeff)
        return x.matmul(p), ColCon, RowCon, SharpCon, DerivSharp

    
    @staticmethod
    def backward(ctx, grad_output, Cconst, Rconst, Sharp, DerivSharp):
        
        x, p, DerCol, DerRow, Sharp, DerSharp, coeff  = ctx.saved_tensors
        

        grad_cnn = (grad_output.matmul(x.transpose(3,2))).sum(dim=0, keepdim=True) #transpose(3,2)
       
        grad_col = grad_cnn + coeff[0]*DerCol
        grad_row = grad_cnn + coeff[1]*DerRow      
        
        if torch.isnan(grad_col.max()):
            print("grad_col is infinite")
            sys.exit()
        if torch.isnan(grad_row.max()):
            print("grad_row is infinite")
            sys.exit()
        
        grad_p = grad_col + grad_row #+ grad_sharp

        return None, grad_p, None, None, None, None, None, None, None, None

PermLay = PermutationLayer.apply

class ApplyPermLayer(torch.nn.Module):
    def __init__( self, perm_size, num_perm, coeff, gamma):
        super(ApplyPermLayer, self).__init__()
        self.coeff = coeff    
        self.register_parameter("pm", torch.nn.Parameter(
                torch.randn(1, num_perm, perm_size, perm_size).sigmoid() * gamma))
        
#        print(self.pm[0,0,:,:].sum(dim=1).max().item())
#        sys.exit()
        self.mu = self.pm.sum(dim=3).max().data.int() + 1
        
#        save_image(self.pm[0,0,:,:], 'Initial_P_Matrix.png')
    
    def forward(self, input):
        RowConstraint, ColConstraint, RowConstraintder, ColConstraintder = self.Constraints()
        Sharpness, dSharpness = self.Sharping()
        return PermLay(input, self.pm, self.coeff, RowConstraint, ColConstraint, RowConstraintder, ColConstraintder, Sharpness, dSharpness)
    
    
    def Sharping(self):
        a = (1/(self.pm.max().item())**3)*(self.pm**2 - self.pm.max().item() * self.pm)**2
        da = (1/(self.pm.max().item())**3)*(self.pm**2 - self.pm.max().item() * self.pm)*(2*self.pm - self.pm.max().item())
                
        return a, da
    
    
    def Barrier(self, Rowc, Colc): #### just for each element's constraint
        # Row = self.mu * (torch.log(1 + (1-Rowc)/self.mu))**2
        # Col = self.mu * (torch.log(1 + (1-Colc)/self.mu))**2
        
        # modified barrier
        Row = self.mu * (torch.log(1+(1-Rowc)/self.mu))
        Col = self.mu * (torch.log(1+(1-Colc)/self.mu))
        
        # # classic barrier squared
        # Row = (self.mu * torch.log(1-Rowc))**2
        # Col = (self.mu * torch.log(1-Colc))**2
        
        return Row.sum(dim=2, keepdim=True), Col.sum(dim=3, keepdim=True)
    
    def Barrderiv(self, Rowc, Colc, Rowdsig, Coldsig):
        # Row = -2 * (1/(1 + (1-Rowc)/self.mu)) * torch.log(1 + (1-Rowc)/self.mu)
        # Col = -2 * (1/(1 + (1-Colc)/self.mu)) * torch.log(1 + (1-Colc)/self.mu)
        
        Row = 1/(self.mu + 1 - Rowc)
        Col = 1/(self.mu + 1 - Colc)
        
        # #classic barrier squared
        # Row = self.mu* (1/(1-Rowc))*torch.log(1-Rowc)
        # Col = self.mu* (1/(1-Colc))*torch.log(1-Rowc)
        return Row,Col

    
    def Constraints(self): ## for doing backpropagation
        BarrierCol=0
        BarrierRow=0
        
        DerivBarrierCol=0
        DerivBarrierRow=0
        
        Rowsummed = self.pm.sum(dim=3, keepdim=True)
        Colsummed = self.pm.sum(dim=2, keepdim=True)
        Rowdersig = self.pm * (1 - self.pm).sum(dim=3, keepdim=True)
        Coldersig = self.pm * (1 - self.pm).sum(dim=2, keepdim=True)
        
        #Ci(x) = 1-Rowsummed , 1-Colsummed
        Rowv, Colv = self.Barrier(Rowsummed, Colsummed)
        derrows, dercols = self.Barrderiv(Rowsummed, Colsummed, Rowdersig, Coldersig)
        
        # backprop to PM weights
        BarrierCol+=Colv
        BarrierRow+=Rowv
        DerivBarrierCol+=dercols
        DerivBarrierRow+=derrows
        
        return BarrierRow, BarrierCol, DerivBarrierRow, DerivBarrierCol




class NET(torch.nn.Module):
    def __init__(self, num_perm, coeff, gamma):
        super(NET, self).__init__()
        
        ##do it seperately like following:
        self.PermLayer = ApplyPermLayer(perm_size=52, num_perm=num_perm, coeff=coeff, gamma=gamma)
        
        self.seq1 = nn.Sequential(
                torch.nn.Conv2d(num_perm, 48, kernel_size=( 5,5 ), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(48),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU()
                )
        self.seq2 = nn.Sequential(              
                torch.nn.Conv2d(48, 48, kernel_size=( 5,5 ), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(48),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU()
                )
        self.seq3 = nn.Sequential(              
                torch.nn.Conv2d(48, 48, kernel_size=(5,5), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(48),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU(),
                )
        self.seq4 = nn.Sequential(              
                torch.nn.Conv2d(48, 48, kernel_size=(5,5), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(48),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU(),
                )
        self.seq45 = nn.Sequential(              
                torch.nn.Conv2d(96, 96, kernel_size=(5,5), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(96),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU(),
                )
        
        self.seq5 = nn.Sequential(
                nn.Linear(55488, 2000), #96 3lyrs: 110976, 5lyrs: 46464
                                         #48 3lys: 55488 4lyrs: 
                nn.ReLU(),
                nn.Dropout(p=0.3),
                
                # nn.Linear(5000, 1000),
                # nn.ReLU(),
                # nn.Dropout(0.2),
                
                nn.Linear(2000, 21),
                nn.ReLU()
                )
        
        
    def forward(self, x):
        
        out, ColBarr, RowBarr, Sharp, DerSharp = self.PermLayer(x) #,perm
        
        out = self.seq1(out)
        out = self.seq2(out) 
        out = self.seq3(out) 
        out = self.seq4(out)
        # out = self.seq45(out)
        out = out.reshape(out.size(0), -1)
        out = self.seq5(out) 
        


        return out, ColBarr, RowBarr, Sharp, DerSharp
    
    
    
    def init_weights(self):
        for m in self.modules():
            
            if type(m) is ApplyPermLayer:
                print('...initializing the PL values NOT DONE')
                print('======================================')
                       
            if type(m) is nn.Conv2d:
                print('...initializing the Conv2d.weights')
                
                for name, param in m.named_parameters():
                    if 'weight' in name:
                        weight_init.xavier_uniform_(param.data)
#                        weight_init.xavier_normal_(param.data)
                    if 'bias' in name:
                        weight_init.zeros_(param.data)
                        
 
            if type(m) is nn.Linear:
                print('...initializing the Linear.weights')
                
                for name, param in m.named_parameters():
                    if 'weight' in name:
                        weight_init.kaiming_normal_(param.data)
                    if 'bias' in name:
                        weight_init.zeros_(param.data)



    def PM_match(self, dim=0):
        if dim == 0:
            #argmax on rows
            #global rowargmax
            rowargmax = self.CEasyargmax(self.PermLayer.pm.sigmoid())###############sigmoid_()#########3
                        
            # zero_init to self.pm
            self.PermLayer.pm = torch.nn.Parameter(torch.zeros((
                            self.PermLayer.pm.shape[0],self.PermLayer.pm.shape[1],
                            self.PermLayer.pm.shape[2],self.PermLayer.pm.shape[3]
                            ))
            )
            
            for channel in range(self.PermLayer.pm.shape[1]):
                for row in range(self.PermLayer.pm.shape[2]):
                    self.PermLayer.pm[0, channel, row, rowargmax[channel,row].item()]=1
                    
                    if torch.cuda.is_available():
                        self.PermLayer.pm.to(_DEVICE)
            
        elif dim == 1:
            #argmax on cols
            colargmax = self.CEasyargmax(self.PermLayer.pm.transpose(3,2))
            # zero_init to self.pm
            self.PermLayer.pm = torch.nn.Parameter(torch.zeros((
                            self.PermLayer.pm.shape[0],self.PermLayer.pm.shape[1],
                            self.PermLayer.pm.shape[2],self.PermLayer.pm.shape[3]
                            ))
            )
            
            for channel in range(self.PermLayer.pm.size()[1]):
                for row in range(self.PermLayer.pm.size()[2]):
                    self.PermLayer.pm[0 ,channel, row, colargmax[channel,row].item()] = 1
                    
                    if torch.cuda.is_available():
                        self.PermLayer.pm.to(_DEVICE)
        else:
            print("dim either 0 or 1.")
            sys.exit()
            
            
            
    def CEasyargmax(self, arr):
            
        mmidx = []    
        for ci, pm in enumerate(arr[0]):
            midx=[]
            for i in range(pm.size(0)):
                li = pm[i]
                max1, max2, min1, min2 = self.Range(li)
                a1 = li==max1
                a2 = li==max2
                index1 = (a1!=0).nonzero()
                index2 = (a2!=0).nonzero()
                
                if (i+1) == pm.size(0):
                    lidx=li.nonzero()              
                    midx.append(lidx.item())
                    break
    
                    
                if index1 not in midx:
                    midx.append(index1.item())
                    
                    if (pm[i+1][0] == True).item() == 0:
                        pm[:,index1]=0
    
                    elif (pm[i+1][0] == True).item() == 1:
                        print('no upcoming row')
                        sys.exit()
                    
    
                elif index1 in midx:
                    if index2 not in midx:
                        midx.append(index2.item())
                    elif index2 in midx:
                        print('inside', index1.item(), midx)
                        sys.exit()
                    continue
            mmidx.append(np.asarray(midx))
        return torch.tensor(np.asarray(mmidx))


    def Range(self, list1):  
        largest = list1[0]   
        largest2 = 0
        lowest = list1[0]
        lowest2 = 0
    
        for item in list1[1:]:      
            if item > largest:  
                largest2 = largest 
                largest = item  
            elif largest2 == 0 or largest2 < item:  
                largest2 = item
            if item < lowest:  
                lowest2 = lowest 
                lowest = item
            elif lowest2 == 0 or lowest2 > item:  
                lowest2 = item
        return largest, largest2, lowest, lowest2
    


        
        
        
        
        
        
        
class ConvNet(nn.Module):
    def __init__(self):
        super(ConvNet, self).__init__()
        
        self.seq1 = nn.Sequential(
                torch.nn.Conv2d(1, 48, kernel_size=( 5,5 ), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(48),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU()
                )
        self.seq2 = nn.Sequential(              
                torch.nn.Conv2d(48, 48, kernel_size=( 5,5 ), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(48),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU(),
                )
        self.seq3 = nn.Sequential(              
                torch.nn.Conv2d(48, 48, kernel_size=(5,5), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(48),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU(),
                )
        self.seq4 = nn.Sequential(              
                torch.nn.Conv2d(48, 48, kernel_size=(5,5), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(48),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU(),
                )
        self.seq45 = nn.Sequential(              
                torch.nn.Conv2d(96, 96, kernel_size=(5,5), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(96),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU(),
                )
        
        self.seq5 = nn.Sequential(
                nn.Linear(55488, 2000),
                nn.ReLU(),
                nn.Dropout(),
                
                # nn.Linear(5000, 1000),
                # nn.ReLU(),
                # nn.Dropout(),
                
                nn.Linear(2000, 21),
                nn.ReLU()
                )
        
        
    def forward(self, x):
           
        out = self.seq1(x)
        out = self.seq2(out) 
        out = self.seq3(out) 
        out = self.seq4(out) 
        # out = self.seq45(out) 
        
        out = out.reshape(out.size(0), -1)
        out = self.seq5(out)
        
        return out
    
    def WriteTimeStart(self):
            return None
    
    def WriteTimeEnd(self, retrain=True):
            return None
    




