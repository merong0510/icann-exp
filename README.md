# ICANN experiments

## Permutation Learning in Convolutional Neural Networks for Time Series Analysis

###### Faculty of Electrical Engineering
###### Department of Automation Technology

Author: Gavneet Singh Chadha, Jinwoo Kim and Andreas Schwung.\
Submitted on May 6th, 2020.

###### Abstract
This study proposes a novel module in the convolutional neural networks (CNN) framework named permutation layer. With the new layer, we are particularly targeting time-series tasks where 2-dimentional CNN kernel loses its ability to capture the spatially co-related features.
Multiveriate time series analysis consists of stacked input channels without considering the order of the channels resulting in an unsorted "2D-image". 2D convolution kernels are not efficient at capturing features from these distorted as the tim series lacks spatial information between the sensor channels.
To overcome this weakness, we propose learnable permutation layers as an extention of vanilla convolution layers which allow to interchange different sensor channels such that sensor channels with similar information content are brought together to enable a more effective 2D convolution operation.
We test the approach on a bench-mark time-series classification task and report the superior performance and applicability of the proposed method.
