# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 13:12:48 2020

@author: at-lab
"""





from __future__ import print_function
import torch
import torch.nn as nn

import torch.utils.data as data 
import torchvision.transforms as transforms
from torch.optim.optimizer import Optimizer, required


import os, sys 

import numpy as np
import matplotlib.pyplot as plt
from torchvision.utils import save_image
import time



import Reporterplot.eleven as eleven
# import Nets_binary as Nets
import Nets_diverse_channels as Nets
# import Nets
# os.chdir('./')

global _DEVICE
_DEVICE=torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(_DEVICE)

#or os.name
#if sys.platform == 'win32':
#    dir_before_src = os.path.abspath(os.path.dirname(sys.argv[0])).split('src')[0]
#    PATH_APPEND = 'src\\lib\\dataset'
#    PATH_TO_DATASET = dir_before_src + PATH_APPEND
#    
#if sys.platform == 'linux':
#    dir_before_src = os.path.abspath(os.path.dirname(sys.argv[0])).split('src')[0]
#    PATH_APPEND = 'src/lib/dataset'
#    PATH_TO_DATASET = dir_before_src + PATH_APPEND




from tempTE_dataset_Rdata_small import TE_process
trans = None
trainset = TE_process(train=True, transform=trans, target_transform=trans, height=52)
testset = TE_process(train=False, transform=trans, target_transform=trans, height=52)

# from tempTE_dataset_Rdata_binary_individual import TE_process
# trans = None
# trainset = TE_process(train=True, transform=trans, target_transform=trans, height=52, FaultNum=2 )
# testset = TE_process(train=False, transform=trans, target_transform=trans, height=52, FaultNum=2 )
# ( self, train=True, transform=None, target_transform=None, height=52, FaultNum=None )
is_cuda_available = torch.cuda.is_available();

# sys.exit()
##sharpening back-prop
class cNewton(Optimizer):
    def __init__(self, params):
        defaults = dict(lr=0)
        self.param_groups = []
        super(cNewton, self).__init__(params, defaults)
        
        param_groups = list(params)
        if len(param_groups) == 0:
            raise ValueError("optimizer got an empty parameter list")
            
        if not isinstance(param_groups[0], dict):
            param_groups = [{'params':param_groups}]
        
        
    def step(self, closure=None):
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    print("No p.grad!!")
                    sys.exit()
                    continue
                d_p = p.grad.data
                p.data.add_(-1, d_p)
#                print("cNewton",d_p.max())
        return loss
    
    def sharpstep(self, sgrad, omega, closure=None):
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:           
            for p in group['params']:
                if p.grad is None:
                    print("No p.grad!!")
                    sys.exit()
                    continue
                d_ps = sgrad.data
                p.data.add_(-omega, d_ps)
        return loss


##PM instance for testing
def start_hooks(model, PM, hooks, somein):
    def get_activation(name, somein):
        def hook(layer, input, output):
            modified = input[0].matmul(somein)
            layer.pm = torch.nn.Parameter(somein)
            PM[name] = somein.data.detach()
            return modified, output[1], output[2], output[3], output[4]
        return hook
    
    for name, layer in model.named_modules():
        if 'PermLayer' in name:
            # print("\tGenerating forward Hook for layer - ", name)
            hooks[name] = layer.register_forward_hook(get_activation(name, somein))

def remove_hooks(hooks):
    for hook in hooks.values():
        hook.remove()

 
pmm={}
hooks={}


def TestNet(net, tester, criterion, optims):
    correct=0
    samples=0
    loss=0
    batch=0
    
    
    # PM Matching
    if optims["CSGD"] != None:
        #PM to PM^prime
        net.PM_match(dim=0)
        start_hooks(net, pmm, hooks, net.PermLayer.pm) #.to(torch.device(type='cpu'))
        
    net = net.to(_DEVICE)
    net.eval()
    for _, (dat, label) in enumerate(tester):
        if is_cuda_available == True:
            dat=dat.to(_DEVICE)
            label=label.to(_DEVICE)

        if optims["CSGD"] != None:
            outs, ColBar, RowBar, sharpness, dersharpness = net(dat)
            loss_sharp = sharpness.sum()
            loss_barrier = ColBar.sum() + RowBar.sum()

        if optims["CSGD"] == None:
            outs=net(dat)
            
        correct+=torch.sum(torch.argmax(outs, dim=1)==label)
        samples+=dat.shape[0]
        loss+=criterion(outs, label.long()).item()
        batch+=1

    if optims["CSGD"] != None:
        remove_hooks(hooks)
    net.train()
    # print('FINISHED TEST SESSION', time.strftime("%Y_%m_%d::%H_%M_%S"))
            
    return float(100.0*correct)/samples, loss/batch


bloss_list=[]
sloss_list=[]
def TrainLoop(net, Optimizers, parameter):
    net = net.to(_DEVICE)
    net.train()
    
    if Optimizers["CSGD"] != None:        
        plt.imshow(net.PermLayer.pm[0,0,:,:].cpu().detach().numpy())
        plt.colorbar()
        fig = plt.gcf()
        fig.savefig('./runs/initial_pm.png')
        plt.clf()
    
    train_loader=parameter["train_loader"]
    test_loader=parameter["test_loader"]
    criterion=parameter["criterion"]
    reporter=parameter["reporter"]
    epoch=parameter["epochs"]
    batch_size = parameter["batch_size"]

    
    optimizer = Optimizers["SGD"]
    if Optimizers["CSGD"] != None:
        c_optimizer = Optimizers["CSGD"]
        omega = parameter["omega"]

    testevery = parameter["testevery"]

    batch=0
    omegacount=0
    

    print('the length of the train:{}, tester:{} '.format( len(parameter["train_loader"]) , len(parameter["test_loader"])) )
    print('testevery:', testevery)
    for ep in range(epoch):
        for _, (dat, label) in enumerate(train_loader):
            if dat.shape[0]!=batch_size:
                continue
            
            if is_cuda_available == True:
                dat=dat.to(_DEVICE)
                label=label.to(_DEVICE)
            
            label=label.long()
                        
            
            if Optimizers["CSGD"] != None:
                outs, ColBar, RowBar, sharpness, dersharpness = net(dat)
                loss_sharp = sharpness.sum()
                loss_barrier = ColBar.sum() + RowBar.sum()
                bloss_list.append(loss_barrier.item())
                sloss_list.append(loss_sharp.item())


            if Optimizers["CSGD"] == None:
                outs = net(dat)
                
            loss=criterion(outs, label)
            optimizer.zero_grad()
            if Optimizers["CSGD"] != None:
                c_optimizer.zero_grad()
            
            
            loss.backward(retain_graph=True)
            if Optimizers["CSGD"] != None:
                loss_barrier.backward()
            
            optimizer.step()
            if Optimizers["CSGD"] != None:
                c_optimizer.step()
                c_optimizer.sharpstep(dersharpness, omega[omegacount])
            
            if Optimizers["CSGD"] != None:
                if (ep*len(train_loader)+_)%round(epoch*len(train_loader)/len(omega)) == 0 and omegacount < len(omega):
                    if _ == 0:
                        continue
                    omegacount += 1
            
            train_acc=100.0*float(torch.sum(torch.argmax(outs, dim=1)==label))/dat.shape[0]

            if batch%testevery==0:
                lasttestloss, lasttestacc=TestNet(net, test_loader, criterion, Optimizers)
            
            if batch%reporter.STEP==0:
                reporter.SetValues([batch, train_acc, loss.item(), lasttestloss, lasttestacc])
            batch+=1

        lasttestloss, lasttestacc=TestNet(net, test_loader, criterion, Optimizers)

    print("\n------ Last Values:")
    reporter.Show()
    print("\n------")
    
    # plt.clf()
    # for i in range(num_perm):
    #     plt.figure(i)
    #     plt.imshow(netlist[0].PermLayer.pm[0,i,:,:].cpu().detach().numpy())
    #     plt.colorbar()
    #     fig = plt.gcf()
    #     fig.savefig('./runs/'+str(condition)+str(i)+'.png')
    #     plt.clf()
    
    
    
    
    
    ###reporter	string
    #parameters for the reporter	a,b,c,d,e,f,g	
    #a: Step (int, Save Numbers every a batches) 
    #b: Avg (int, Smoothening window for average Display) 
    #c: Show (int, Show new Values every c Batches) 
    #d: Line (int, Show new Line every d Batches) 
    #e: Header (int, Show new Header every e Batches)	
    #5,20,25,250,5000	b,c,d,e should be divisible by a
def LoadNets():
    NetList=[]
    Optimizers=[]
    Parameters=[]
    ################################# PCNN #################################
    parameter = {}
    parameter["batch_size"] = 100
    parameter["epochs"] = 50
    parameter["criterion"] = torch.nn.CrossEntropyLoss()
    parameter["train_loader"] = data.DataLoader(trainset, batch_size=parameter["batch_size"], shuffle=True, num_workers=0)
    parameter["test_loader"] = data.DataLoader(testset, batch_size=parameter["batch_size"], shuffle=True, num_workers=0)
    parameter["testevery"] = 50
    
    string = '5,30,50,250,5000'
    parameter["reporter"] = eleven.ReporterFromString(string)
    # Coeff, learning rate, PL channel
    C1=0.5*0.0006
    C2=0.5*0.0006
    global coeff, num_perm, learning_rate, gamma, condition
    

    num_perm = 50
    learning_rate = 0.04
    coeff = torch.tensor([C1,C2], requires_grad=False)
    gamma = torch.tensor([0.3], requires_grad=False)
    
    # parameter["omega"] = torch.tensor([0.001, 0.001, 0.001, 0.001, 0.001], requires_grad=False)
    # condition = "flat omega"
    parameter["omega"] = torch.tensor([0.0025, 0.0073, 0.016, 0.023, 0.03], requires_grad=False)*0.5
    condition = "increasing omega"
    
    
    
    
    model = Nets.NET(num_perm, coeff, gamma)
    model.init_weights()
    if is_cuda_available == True:
        model = model.to(_DEVICE)
    print('Start sum: ', model.PermLayer.pm[0,0,:,:].sum(dim=1)[0:10], '\n')
    
    NetList.append(model)
    Parameters.append(parameter)
    
    optimizer = {}
    # Loss and optimizer optim.Adadelta vs optim.SGD
    optimizer["SGD"] = torch.optim.Adadelta([
                {'params': model.seq1.parameters()},
                {'params': model.seq2.parameters()},
                {'params': model.seq3.parameters()},
                {'params': model.seq4.parameters()},
                {'params': model.seq45.parameters()},
                {'params': model.seq46.parameters()},
                {'params': model.seq47.parameters()},
                ], lr=learning_rate)
    optimizer["CSGD"] = cNewton([
                {'params':model.PermLayer.parameters()}
                ])
    
    Optimizers.append(optimizer)
    
    
    
    
    ################################# CNN #################################
    parameter2 = {}
    parameter2["batch_size"] = 100
    parameter2["epochs"] = 50
    parameter2["criterion"] = torch.nn.CrossEntropyLoss()
    parameter2["train_loader"] = data.DataLoader(trainset, batch_size=parameter2["batch_size"], shuffle=True, num_workers=0)
    parameter2["test_loader"] = data.DataLoader(testset, batch_size=parameter2["batch_size"], shuffle=True, num_workers=0)
    parameter2["testevery"] = 50
    string2 = '5,30,50,250,5000'
    parameter2["reporter"] = eleven.ReporterFromString(string2)
    
    model = Nets.ConvNet()
    if is_cuda_available == True:
        model.to(_DEVICE)
    NetList.append(model)
    Parameters.append(parameter2)
    
    optimizer2 = {}
    optimizer2["SGD"] = torch.optim.Adadelta(model.parameters(), lr = learning_rate)
    optimizer2["CSGD"] = None
    Optimizers.append(optimizer2)
    
    Names = ['PCNN','Vanilla CNN']
    
    
    return NetList, Optimizers, Parameters, Names





#plot function col17lyrs50perms
# lyr='7lyrs50permsada'
lyr = '7lyrsdiversewithmatch'
save_directory='./col1'+lyr

netlist, optimizers, parameters, names = LoadNets()
if len(netlist)>0:
    for netidx in range(len(netlist)):
        net=netlist[netidx]
        optimizer=optimizers[netidx]
        parameter=parameters[netidx]
        
        print("Start train net %i of %i (%s)"%(netidx+1, len(netlist), names[netidx]))
        starttime = time.strftime("%Y_%m_%d::%H_%M_%S")
        
        with open(save_directory + '/results.txt', 'w+') as f:
            f.write('=======Starting time: ' + str(starttime) + '\n\n')
        print('time', starttime)
        TrainLoop(net, optimizer, parameter)
        
        endtime = time.strftime("%Y_%m_%d::%H_%M_%S")
        print('time',endtime)
    figures, figurenames = eleven.PlotResults([x["reporter"] for x in parameters], [name for name in names], smoothing=5)
    
    figures[0].savefig(save_directory + '/col1_loss.png')
    figures[1].savefig(save_directory + '/col1_acc.png')
    # figures[1].savefig('./col1/col1_acc.png')
    
    with open(save_directory + '/results.txt', 'a+') as f:
         
        f.write(str(parameters[0]["reporter"].NAMES)+'\n')
   
        # f.write('\nFault Number ['+str(faultn)+']\n')
        f.write('PCNN \n'
                +str(parameters[0]["reporter"].VALS['Minibatch'][-1])+'\t'
                +str(parameters[0]["reporter"].VALS['Acc'][-1])+'\t'
                +str(parameters[0]["reporter"].VALS['Loss'][-1])+'\t'
                +str(parameters[0]["reporter"].VALS['Test Acc'][-1])+'\t'
                +str(parameters[0]["reporter"].VALS['Test Loss'][-1])+'\t'
                +str(parameters[0]["reporter"].VALS['avg. Loss'][-1])+'\t'
                +str(parameters[0]["reporter"].VALS['avg. Acc'][-1])+'\t'+
                '\n')
        f.write('Vanilla \n'
                +str(parameters[1]["reporter"].VALS['Minibatch'][-1])+'\t'
                +str(parameters[1]["reporter"].VALS['Acc'][-1])+'\t'
                +str(parameters[1]["reporter"].VALS['Loss'][-1])+'\t'
                +str(parameters[1]["reporter"].VALS['Test Acc'][-1])+'\t'
                +str(parameters[1]["reporter"].VALS['Test Loss'][-1])+'\t'
                +str(parameters[1]["reporter"].VALS['avg. Loss'][-1])+'\t'
                +str(parameters[1]["reporter"].VALS['avg. Acc'][-1])+'\t'+
                '\n'+'=======Finishing time: '+str(endtime) + '\n')
        
        
        # if faultn == 10:
        f.write('\n::Run condition::\n'+'Perm_size: '
                +str(netlist[0].PermLayer.pm.shape[1])+'\n'
                +str(netlist[0].seq1[0])+'\n'
                +str(netlist[0].seq2[0])+'\n'
                )
        f.write('======================\nVCNN vs PCNN epoch:{}, batchsize:{} \n' 
                .format(parameters[0]["epochs"], parameters[0]["batch_size"])
                +"gamma for Initial PM:" +str(gamma)
                +"coeff:{},  omega:{}, num_perm:{} \n".format(coeff, parameters[0]["omega"], num_perm)
                +"learningrate: {}".format(learning_rate)
                )
            
        pass

   
    print('======================\nVCNN vs PCNN epoch:{}, batchsize:{} '
          .format(parameters[0]["epochs"], parameters[0]["batch_size"]))
    print("gamma for Initial PM:", gamma)
    print('coeff:{},  omega:{}, num_perm:{} '.format(coeff, parameters[0]["omega"], num_perm))
    print('learningrate: {}'.format(learning_rate))
    
    
    print('======================\nVCNN vs PCNN epoch:{}, batchsize:{} '
          .format(parameters[0]["epochs"], parameters[0]["batch_size"]))
    print("gamma for Initial PM:", gamma)
    print('coeff:{},  omega:{}, num_perm:{} '.format(coeff, parameters[0]["omega"], num_perm))
    print('learningrate: {}'.format(learning_rate))
else:
    print("No nets to train found.")
# plt.clf()
# plt.plot(np.asarray(bloss_list)[:100])
# plt.show()
# plt.close()


parameters[1]={}
parameters[1]["reporter"] = eleven.ReporterFromString('5,30,50,250,5000')
figures, figurenames = eleven.PlotResults([x["reporter"] for x in parameters], [name for name in names], smoothing=5)




# plot the pm^prime
    # netlist[0].PM_match(dim=0)



#####################Lr range test
#import math
#
#end_lr = 0.01
#start_lr = learning_rate
#lr_find_epochs = 3
###Learning rate
#lr_lambda = lambda x: math.exp(x * math.log(end_lr / start_lr)/(lr_find_epochs * len(train_loader)))
#scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda)
#
### lists to capture the logs of Cyclical learning Rates
#lr_find_loss = []
#lr_find_lr = []
#iter = 0
#smoothing = 0.1
#
#acc_list=[]
##for epoch in range(num_epochs):
#for j in range(lr_find_epochs):
#    print("lr_find epoch: {}".format(j))
#    for i, (images, labels) in enumerate(train_loader):
#    
#        if images.shape[0]!=batch_size:
#            continue
#        
#        #forward+backward+optimize
#        outputs = model(images)
#        
#        labels.view(-1)
#        loss = criterion(outputs, labels)
#        
#        #zero the parmeter gradients
#        optimizer.zero_grad()
#        loss.backward()
#        optimizer.step()
#        
#        
#        if SLR == True:
#            scheduler.step()
#            
#            lr_step = optimizer.state_dict()["param_groups"][0]["lr"]
#            lr_find_lr.append(lr_step)
#            
#            
#            if iter == 0:
#                lr_find_loss.append(loss.item())
#            else:
#                loss = smoothing * loss + (1-smoothing)*lr_find_loss[-1]
#                lr_find_loss.append(loss.item())
#            iter+=1
#            
#            
#            print(' Step [{}/{}], Loss: {:.4f}, lr_find_lr: {:.4f}'
#              .format( i + 1, total_step, loss.item(), lr_step))
#            
#            
#            #### criterion
#    #        if (correct/total) >= acc_list[-2]: # whether [-2] is right should be checked
#                #select the learing rate.     
#        
#        
#        total = labels.size(0)
#        _, predicted = torch.max(outputs.data, 1)                         ########## 1. whether the outputs ( outputs.max() ) consistent with prediction.    2. prediction and labels-> correct  affects or not?
#        
#        correct = (predicted == labels).sum().item()
#        acc_list.append(correct / total)
#        
##        if (i + 1) % 50 == 0:
#        print(' Step [{}/{}], Loss: {:.4f}, Accuracy: {:.2f}% '
#              .format( i + 1, total_step, loss.item(), (correct / total) * 100))
#        
#        
#plt.plot(np.asarray(lr_find_lr), np.asarray(lr_find_loss))
#        
        



         


'''
test_losses = []

model.eval()
correct = 0 
with torch.no_grad():
    for ti, (img, target) in enumerate(test_loader):
        output = model(img)
        test_loss = criterion(output, target).item()
        test_losses.append(test_loss)
        pred = output.data.max(1, keepdim=False)[1]
        
        batch_total = target.size(0)
        batch_correct = (pred == target).sum().item()
        
        correct += pred.eq(target.data.view_as(pred)).sum()
        
        print("Step [{}/{}], Loss:{:.4f}, Acc:{:.2f}%" .format(ti+1, len(test_loader), test_loss, (batch_correct / batch_total)*100))
    
    test_losses = np.asarray(test_losses).mean()
    print("\nTest set: Avg. loss: {:.4f}, Acc: {}/{} ({:.2f}%)\n" .format(test_losses, correct, len(test_loader.dataset), 100.* correct / len(test_loader.dataset) ))




'''
 

