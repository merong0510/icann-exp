# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 19:47:24 2019

@author: merong0510
@TE data preprocessing
"""

from __future__ import print_function
import torch

import numpy as np
from sklearn import preprocessing

import torch.utils.data as data

import pyreadr
import matplotlib.pyplot as plt

import os, sys

#https://www.programcreek.com/python/example/107691/torch.nn.Conv2d

## ======================= column variables ======================================================
## ['faultNumber', 'simulationRun', 'sample', 'xmeas_1', 'xmeas_2',
##        'xmeas_3', 'xmeas_4', 'xmeas_5', 'xmeas_6', 'xmeas_7', 'xmeas_8',
##        'xmeas_9', 'xmeas_10', 'xmeas_11', 'xmeas_12', 'xmeas_13', 'xmeas_14',
##        'xmeas_15', 'xmeas_16', 'xmeas_17', 'xmeas_18', 'xmeas_19', 'xmeas_20',
##        'xmeas_21', 'xmeas_22', 'xmeas_23', 'xmeas_24', 'xmeas_25', 'xmeas_26',
##        'xmeas_27', 'xmeas_28', 'xmeas_29', 'xmeas_30', 'xmeas_31', 'xmeas_32',
##        'xmeas_33', 'xmeas_34', 'xmeas_35', 'xmeas_36', 'xmeas_37', 'xmeas_38',
##        'xmeas_39', 'xmeas_40', 'xmeas_41', 'xmv_1', 'xmv_2', 'xmv_3', 'xmv_4',
##        'xmv_5', 'xmv_6', 'xmv_7', 'xmv_8', 'xmv_9', 'xmv_10', 'xmv_11']
## =============================================================================


class TE_process(data.Dataset):
    
#    if sys.platform == 'win32':
#        os.chdir('C:\\Users\\merong0510\\sciebo\\From Jinwoo\\src')
#        dir_before_src = os.path.abspath(os.path.dirname(sys.argv[0])).split('src')[0]
#        PATH_APPEND = '\\src\\lib\\dataset\\'
#        PATH_TO_DATASET = dir_before_src + PATH_APPEND
#    
#    if sys.platform == 'linux':
#        os.chdir('/home/at-lab/Desktop/VMshare/src')
#        dir_before_src = os.path.abspath(os.path.dirname(sys.argv[0])).split('src')[0]
#        PATH_APPEND = 'src/lib/dataset/Rdata/'
#        PATH_TO_DATASET = dir_before_src + PATH_APPEND
    
    #server
    os.chdir('/media/NAS/Kim/PCNN/')
    dir_before_src = os.path.abspath(os.path.dirname(sys.argv[0])).split('lib')[0]
    PATH_APPEND = '/lib/dataset/Rdata/'
    PATH_TO_DATASET = dir_before_src + PATH_APPEND
    
    scaler = preprocessing.MinMaxScaler()
    
    def __init__( self, train=True, transform=None, target_transform=None, height = 52 ):
        self.transform = transform
        self.target_transform = target_transform
        self.train = train        
        self.height = height
        
        if self.train:           
            load = pyreadr.read_r(self.PATH_TO_DATASET + 'TEP_FaultFree_Training.RData')
            load = load['fault_free_training']
            
            self.data_train = self.scaler.fit_transform(np.asarray(load, dtype=np.float32)[:,3:])
            self.data_train = torch.Tensor(self.data_train)
            
            temp_load = pyreadr.read_r(self.PATH_TO_DATASET + 'TEP_Faulty_Training.RData') #TEP_Faulty_Training
            temp_load = temp_load['faulty_training'] #faulty_training
            temp_data = self.scaler.fit_transform(np.asarray(temp_load, dtype=np.float32)[:,3:])
            temp_data = torch.Tensor(temp_data)
            
            #images
            self.data_train = torch.cat([self.data_train, temp_data])
            ######################################
            ######################################
            self.data_train=self.data_train.flip(0)
            while self.data_train.size()[0] % self.height != 0:
                self.data_train=self.data_train.narrow(0,1,self.data_train.size()[0]-1)
            self.data_train=self.data_train.flip(0)
            ######################################
            ######################################
            # self.data_train = self.data_train[:729976,:]
            ######################################
            ######################################
            
            #label
            self.label_train = torch.Tensor(np.asarray(load, dtype=np.int16)[:,0])
            temp_load_label = torch.Tensor(np.asarray(temp_load, dtype=np.float32)[:,0])
            self.label_train = torch.cat([self.label_train, temp_load_label])
            del(temp_load_label)
            ######################################
            ######################################
            self.label_train = self.label_train[:self.data_train.size()[0]]
            ######################################
            ######################################
            
            #reshape
            self.data_train = np.asarray(self.data_train).reshape(-1,1,self.height,52)
            self.label_train = self.label_train.reshape(-1,self.height)
            self.label_train = self.label_train[:,0]
            
            
        else:
            load = pyreadr.read_r(self.PATH_TO_DATASET + 'TEP_FaultFree_Testing.RData')
            load = load['fault_free_testing']
            
            self.data_test = self.scaler.fit_transform(np.asarray(load, dtype=np.float32)[:,3:])
            self.data_test = torch.Tensor(self.data_test)
            
            temp_load = pyreadr.read_r(self.PATH_TO_DATASET + 'TEP_Faulty_Testing.RData')
            temp_load = temp_load['faulty_testing']
            temp_data = self.scaler.fit_transform(np.asarray(temp_load, dtype=np.float32)[:,3:])
            temp_data = torch.Tensor(temp_data)
            
            #images
            self.data_test = torch.cat([self.data_test, temp_data])
            ######################################
            ######################################
            self.data_test=self.data_test.flip(0)
            while self.data_test.size()[0] % self.height != 0:
                self.data_test=self.data_test.narrow(0,1,self.data_test.size()[0]-1)
            self.data_test=self.data_test.flip(0)
            ######################################
            ######################################
            # self.data_test = self.data_test[:10079992, :]
            ######################################
            ######################################
            
            
            #label
            self.label_test = torch.Tensor(np.asarray(load, dtype=np.int16)[:,0])
            temp_load_label = torch.Tensor(np.asarray(temp_load, dtype=np.float32)[:,0])
            self.label_test = torch.cat([self.label_test, temp_load_label])
            del(temp_load_label)
            ######################################
            ######################################
            self.label_test = self.label_test[:self.data_test.size()[0]]
            ######################################
            ######################################
            
            
            #reshape
            self.data_test = np.asarray(self.data_test).reshape(-1,1,self.height,52)
            self.label_test = self.label_test.reshape(-1,self.height)
            self.label_test = self.label_test[:,0]
        

    def __getitem__(self, index):
        
        if self.train:
            img, target = self.data_train[index], self.label_train[index].long()
            
        else:
            img, target = self.data_test[index], self.label_test[index].long()

        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            target = self.target_transform(target)
            
        return img, target

    def __len__(self):
        if self.train:
            return len(self.data_train)
        else:
            return len(self.data_test)



 
